#!/usr/bin/env python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
This is the client for usearch server. 

Please run worker.py before run this. You also need to specify the hostname of running 
rabbitmq-server to run this.

* Steps
1. The client takes user's query, compresses it, and sends to the broker
2. The broker maintains a queue (channel) for the queries from multiple users in FIFO style and also dispatch each query evenly to the consumers (round robin).
3. Each consumer waits for a task to be assigned. Once the broker sends one, it decompress the message and creates a query file in "q.dir" to notify the usearch server to start work.
4. The output will be created under "out.dir" 

"""
import pika
import sys
import gzip
import cPickle
import zlib

def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)
    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr)) 

def send_query(borkerHostName, query):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=borkerHostName))
	channel = connection.channel()

	channel.queue_declare(queue='task_queue', durable=True)

 	msgContainer = {}
 	msgContainer["qid"] = 1
 	msgContainer["query"] = query
 	## Currently this "options" is not used. 
 	msgContainer["options"] = "-server -ublast `pwd`/q.dir -db refGenomes.udb -evalue 1e-2 -blast6out results.b6 -query_seqtype amino --quiet" 
 	msg_zipped = zdumps(msgContainer)
 	 
	channel.basic_publish(exchange='',
    	                  routing_key='task_queue',
        	              body=msg_zipped,
            	          properties=pika.BasicProperties(
                	         delivery_mode = 2, # make message persistent
                    	  ))
	print " [x] Sent %r" % (msgContainer,)
	connection.close()

def usage():
    print "Usage: python new_task.py <rabbitmq_server_hostname>"

def main(argv):
    """
    Parse the command line inputs and call send_info 

    @param argv: list of arguments
    @type argv: list of strings
    """
    
    assert (len(sys.argv) > 1)

	##
	## The user's query can be passed in many ways. For testing, I've just hardcoded one query. 
	##
    aQuery = ">WSSedB1BDRAFT_00000150"
    aQuery += "\nMYRITLILLFIMVPSCKSWTPGNKVTSADLKWSVKMANSIINRSDSLIYYVDRNPKWAYDVAFLGMAIDRLGDIDPKYSKYMEDWVNYFVGPDGSIIDYRKEEYNLDRLFPGRNVMTLYQRTNDIRYRYALGNLLGQLKDHPKTKSGGYWHKKIYPWQMWLDGIFMGSTFMVQCASVFDAPEWYDIACNQVKMIYEKTLDQNTGLLMHAWDESRQQKWCDPETGKSHYPWSRATGWYTMAIIDILEYLPQDHPDRDELIEILNKTCEAILKVQDKKSGLWFQVLNQGGREGNYIEGSGSAMFSYVFARGARKGYLDKKFIRIAGKAFDDIIRVLITTDEKGFLTMHNICGGCGLGGNPYRDGSYEYYINEKRVDNDTKGVAPFILAAIELDR*"
 	#print aQuery
	
	## send a fasta query to server
    send_query(sys.argv[1], aQuery)

if __name__ == "__main__":
    main(sys.argv)   

# EOF