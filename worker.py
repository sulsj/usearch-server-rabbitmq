#!/usr/bin/env python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
This is the consumer. 

Please run rabbitmq-server first. You should specify the hostname to run this.

* Steps
1. The client takes user's query, compresses it, and sends to the broker
2. The broker maintains a queue (channel) for the queries from multiple users in FIFO style and also dispatch each query evenly to the consumers (round robin).
3. Each consumer waits for a task to be assigned. Once the broker sends one, it decompress the message and creates a query file in "q.dir" to notify the usearch server to start work.
4. The output will be created under "out.dir" 

"""

import pika
import time
import sys
import gzip
import cPickle
import zlib
import datetime
import os
import subprocess

import portalocker

def zdumps(obj):
    """
    dumps pickleable object into zlib compressed string
    """
    return zlib.compress(cPickle.dumps(obj,cPickle.HIGHEST_PROTOCOL),9)
    
def zloads(zstr):
    """
    loads pickleable object from zlib compressed string
    """
    return cPickle.loads(zlib.decompress(zstr)) 

def usage():
    print "Usage: python worker.py <rabbitmq_broker_hostname>"

def run_usearch(brokerHostName):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=brokerHostName))
	channel = connection.channel()
	
	channel.queue_declare(queue='task_queue', durable=True)
	print ' [*] Waiting for messages. To exit press CTRL+C'

	def callback(ch, method, properties, body):
		msg_unzipped = zloads(body)
		
		qid = msg_unzipped["qid"]
		query = msg_unzipped["query"]
		options = msg_unzipped["options"] # This is ignored currently.
		
		print " [x] Received %s" % (query,)
		time.sleep(1) # Is this necessary?
		
		##
		## lock a new file and write the query into the file and unlock.
		## This prevents usearch64 tried to read the file before the query file writing is completed.
		##
		workingDir =  os.getcwd()
		now = datetime.datetime.now()
		qFileName = now.strftime("%Y-%m-%d-%H-%M-%S-%f") + "_locked.faa"
		fp = open(workingDir + "/" + qFileName, "w")
		try:
			portalocker.lock(fp, portalocker.LOCK_EX | portalocker.LOCK_NB)
		except portalocker.LockException:
		   	# File locked by somebody else. Do some other work, then try again later.
		   	pass
		else:
		   	# Lock acquired.  Do your thing
		   	try:
				fp.write(query)
		   	finally:
			  	portalocker.unlock(fp)
		fp.close()
		
		##
		## Example for creating query and output setting file in "q.dir"
		## : echo /serverfiles/queries/q123.fasta /serverfiles/results/out123 > query_dir/123.q
		##
		pFileName = qFileName + ".q"
		p = subprocess.Popen("mkdir " + workingDir + "/out.dir/" + qFileName + ".out", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		p.wait() ## wait for mkdir 
		fp = open(workingDir + "/q.dir/" + pFileName, "w")
		fp.write(workingDir + "/" + qFileName + " " + workingDir + "/out.dir/" + qFileName + ".out")
		fp.close()
		
		##
		## need to remove query file?
		##
		print " [x] Done"
		ch.basic_ack(delivery_tag = method.delivery_tag)
	
	channel.basic_qos(prefetch_count=1)
	channel.basic_consume(callback, queue='task_queue')
	
	channel.start_consuming()

def main(argv):

	workingDir =  os.getcwd()
	
	##
	## Start usearch64 in server mode
	##
	#usearchServerCmd = "./usearch64 -server -ublast " + str(workingDir) + "/q.dir -db " \
 	#	+ str(workingDir) + "/test.faa.udb -blast6out test.b6 -query_seqtype amino -evalue 1"
 	#print usearchServerCmd
 	#p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #stdout_value = p.communicate()[0]
    #print debugmsg(stdout_value)
    #retval = p.wait()
	
	assert (len(sys.argv) > 1)
	run_usearch(sys.argv[1])
	#p.wait() # if stop.q is created in "q.dir", usearch will exit itself.
	
if __name__ == "__main__":
    main(sys.argv)   

# EOF