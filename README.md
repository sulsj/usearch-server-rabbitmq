usearch-server-rabbitmq
=======================

Multi usearch64 server with RabbitMQ


Testing the usearch64 server version with a reliable message queue. 
You can see the configuration here:

https://docs.google.com/a/lbl.gov/presentation/d/1PK80IA3xGb52u24Q7NPX_Hy9Su6N39BEMAFjBt_gW6k/edit

Operation steps

1. The client takes user's query, compresses it, and sends to the broker
2. The broker maintains a queue (channel) for the queries from multiple users in FIFO style and also dispatch each query evenly to the consumers (round robin).
3. Each consumer waits for a task to be assigned. Once the broker sends one, it decompress the message and creates a query file in "q.dir" to notify the usearch server to start work.
4. The output will be created under "out.dir"

Note

- Basically you can add as many usearch servers as you want if you have that many licenses.
- Consumer can start the usearch server with user specified search options if needed.
- The distribution among servers can be directed by checking the load of each server (Again currently it's round robin).
- The consumer can send the search result back to the client using a separate channel in the message broker so that the client receives "the" result. It is also possible to shard the user query file, run usearch servers, and merge all output files into one (for tabular output format). 
